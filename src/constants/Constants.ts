export const CRYPTO_BALANCE_TRACKER_URL = "http://localhost:8080/api/v1";
export const DASHBOARDS_ENDPOINT = CRYPTO_BALANCE_TRACKER_URL.concat("/dashboards");
export const MONGO_ID_REGEX = /^[a-zA-Z0-9]{24}$/;