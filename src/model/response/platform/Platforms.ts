export interface Platforms {
  platformName: string,
  percentage: number,
  balance: bigint
}