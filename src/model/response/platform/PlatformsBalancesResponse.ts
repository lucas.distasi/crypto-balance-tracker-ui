import {Platforms} from "./Platforms";

export interface PlatformsBalancesResponse {
  platforms: Platforms[],
}