export interface GoalResponse {
  goalId: string
  cryptoName: string
  actualQuantity: string
  progress: number
  remainingQuantity: string
  goalQuantity: string
  moneyNeeded: string
}