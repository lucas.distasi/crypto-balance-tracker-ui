import {DashboardCoinInfoResponse} from "./DashboardCoinInfoResponse";

export interface CryptosBalancesPlatformsResponse {
  coinInfoResponse: DashboardCoinInfoResponse[]
}