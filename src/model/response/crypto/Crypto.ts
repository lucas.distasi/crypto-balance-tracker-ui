export interface Crypto {
  coinId: string,
  coinName: string,
  platform: string,
  quantity: string
}